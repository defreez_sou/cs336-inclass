#!/usr/bin/env python
"""The Net1 P2P client."""

import starter
import argparse

def main():
    """Our main function. Put your code here."""
    args = parse_args()
    local_ip = args.b
    local_port = args.p

def parse_args():
    """Return parsed command line arguments."""
    parser = argparse.ArgumentParser(description="NetI P2P Network")
    parser.add_argument('-b', help="Local bind IP", required=True)
    parser.add_argument('-p', help="Local bind port", required=True, type=int)
    parser.add_argument('-t', help="IP of tracker")
    parser.add_argument('-r', help="Port of tracker", type=int)
    parser.add_argument('-f', help="Name of file to seed")
    args = parser.parse_args()

    return args

if __name__ == "__main__":
    main()
