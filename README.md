cs336-p2p
=========

I have moved the starter code for the p2p project into the starter folder. You
should use the p2ppy file in that directory as the starting point for your lab.
Check for updates with git pull! I will probably add hints to starter.py that
will make your life easier.

The example code for builing a server with select-based multiplexing is in the select folder.
