#!/usr/bin/env python

import socket
import struct

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 1337))

payload = "12345678901"

while 1:
    s.sendall(payload)
    print s.recv(4096)

s.close()
